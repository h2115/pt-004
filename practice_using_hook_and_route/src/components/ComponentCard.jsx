import { Button } from "react-bootstrap";
import React from "react";
import { Card } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";

export default function ComponentCard({ data }) {
  console.log(data);

  return (
    <div>
      <div>
        <Container>
          <Row>
            {data.map((item) => (
              <Col md={3}>
                <Card style={{ width: "18rem" }}>
                  <Card.Img variant="top" src={item.thumbnail} />
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>{item.description}</Card.Text>
                    <h3 style={{ color: "red" }}>
                      <b>{item.price}</b>
                    </h3>
                    <Button variant="info">By the Course</Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </div>
  );
}
