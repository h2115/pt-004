import React from "react";
import { Nav } from "react-bootstrap";
import { Navbar } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export default function ComponentNavBar() {
  return (
    <div>
      <>
        <Navbar Navbar bg="info" variant='light'>
          <Container>
            <Navbar.Brand as={NavLink} to="/">PT-004</Navbar.Brand>
            <Nav className="ml-auto">
              <Nav.Link as={NavLink} to="/home">
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/about">
                About
              </Nav.Link>
              <Nav.Link as={NavLink} to="/our_vision">
                OurVision
              </Nav.Link>
            </Nav>
          </Container>
        </Navbar>
        <br />
      </>
    </div>
  );
}
