import React from "react";
import { Card } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Container } from "react-bootstrap";

export default function () {
  return (
    <div>
      <Container style={{ fontFamily: "Agency FB" }}>
        <Row>
          <Col md={6}>
            <div className="card1">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Vision</Card.Title>
                <Card.Body>
                  <ul>
                    <li>Hight Quality Traning and Research</li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col md={6}>
            <div className="card2">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Mission</Card.Title>
                <Card.Body>
                  <ul>
                    <li>Hight Quality Traning and Research</li>

                    <li>Developing the new ICT Program</li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
        <Row style={{ marginTop: "10px" }}>
          <Col md={6}>
            <div className="card3">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Strategy</Card.Title>
                <Card.Body>
                  <ul>
                    <li>Hight Quality Traning and Research</li>
                    <li>
                      Developing Capacity of SW Experts to be Leaders in IT
                      Field
                    </li>
                    <li>Developing the new ICT Program</li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col md={6}>
            <div className="card4">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Slogan</Card.Title>
                <Card.Body>
                  <ul>
                    <li>Hight Quality Traning and Research</li>
                    <li>
                      Developing Capacity of SW Experts to be Leaders in IT
                      Field
                    </li>
                    <li>Developing the new ICT Program</li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
        <Row style={{ marginTop: "15vh" }}>
          <Col md={6}>
            <div className="card5">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Strategy</Card.Title>
                <Card.Body>
                  <ul>
                    <li>
                      Developing Capacity of SW Experts to be Leaders in IT
                      Field
                    </li>
                    <li>Developing the new ICT Program</li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col md={6}>
            <div className="card6">
              <Card style={{ backgroundColor: "lightgray", height: "150px" }}>
                <Card.Title>Slogan</Card.Title>
                <Card.Body>
                  <ul>
                    <li>
                      Developing Capacity of SW Experts to be Leaders in IT
                      Field
                    </li>
                  </ul>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
