import React from "react";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Container } from "react-bootstrap";
import logo from "./Images/logo.png";

export default function ComponentFooter() {
  return (
    <div>
      <br />
      <Container>
        <Row>
          <Col md={4}>
            <img src={logo} width="80px" height="100px"></img>
            <br/>
          
                <b style={{fontFamily:'Khmer OS Muol Light',fontSize:'12px'}}>®រក្សាសិទ្ធគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ ២០២២</b>
            
          </Col>
          <Col md={4}>
            <h5>Address</h5>
            <p>
              <b>Address:</b> #12, St 323, Sangkat Boeung Kak II, Khan Toul
              Kork, Phnom Penh, Cambodia.
            </p>
          </Col>
          <Col md={4}>
            <h5>Contact</h5>
            <p>
              <b>Tel:</b> 012 998 919 (Khmer)
            </p>
            <p>085 402 605 (Korean)</p>

            <p>
              <b>Email:</b>info.kshrd@gmail.com
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
